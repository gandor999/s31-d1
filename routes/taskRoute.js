// Setup express dependency
const express = require('express');


// Create a Router instance
const router = express.Router();


// Import taskController.js
const taskController = require("../controllers/taskController");







// Routes


// Route to get all tasks
router.get("/", (req, res) => {
	taskController.getAllTasks()
	.then(resultFromController => res.send(resultFromController));
});




// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body)
	.then(resultFromController => res.send(resultFromController));
});




// Route for deleting a task
// localhost:3001/tasks/4509385u34053049
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id)
	.then(resultFromController => res.send(`Deleted:  ${resultFromController}`));
});





// Route for updating a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body)
	.then(resultFromController => res.send(`Updated:  ${resultFromController}`));
});





// Activity

router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id)
	.then(resultFromController => res.send(resultFromController));
});



router.put("/:id/complete", (req, res) => {
	taskController.completeTask(req.params.id, req.body)
	.then(resultFromController => res.send(`Completed:  ${resultFromController}`));
});






module.exports = router;