// Importing the model
const Task = require("../models/task");





// Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}




// Controller function for creating a task
module.exports.createTask = (requestBody) => {

	// Creates a task object based on the Mongoose model "Task"

	let newTask = new Task({

		// Sets the name property with the value recieved from the client /Postman
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			return task;
		}
	});

};




module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId)
	.then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask;
		}
	})
};





module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId)
	.then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		result.name = newContent.name;

		return result.save()
		.then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updatedTask;
			}
		})
	})
}








// Activity

module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}



module.exports.completeTask = (taskId, newContent) => {
	return Task.findById(taskId)
	.then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		newContent.status = "complete"
		result.status = newContent.status;


		return result.save()
		.then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updatedTask;
			}
		})
	})
}